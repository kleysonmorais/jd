import React from "react"

export default props => (
  <svg width="17" height="17" viewBox="0 0 17 17" {...props}>
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#56B979"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
    >
      <path d="M13 9.6v4.8a1.6 1.6 0 0 1-1.6 1.6H2.6A1.6 1.6 0 0 1 1 14.4V5.6A1.6 1.6 0 0 1 2.6 4h4.8M11 1h5v5M6 11L16 1" />
    </g>
  </svg>
)

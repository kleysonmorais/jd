import React from "react"

export default props => (
  <svg
    width="26px"
    height="22px"
    viewBox="0 0 26 22"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <title>Group 13</title>
    <desc>Created with Sketch.</desc>
    <defs />
    <g
      id="Symbols"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="Menu---CMS---Preto"
        transform="translate(-16.000000, -18.000000)"
        strokeWidth="2"
        stroke="#FFFFFF"
      >
        <g id="Group-4">
          <g id="Group-13" transform="translate(16.000000, 18.000000)">
            <rect id="Rectangle-21" x="1" y="1" width="24" height="20" rx="1" />
            <polyline
              id="Line"
              strokeLinecap="square"
              points="8 2 8 16.451573 8 20"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
)

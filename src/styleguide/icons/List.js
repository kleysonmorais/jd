import React from "react"

export default props => (
  <svg
    width="32"
    height="28"
    viewBox="0 0 32 28"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g
      fill="none"
      fillRule="evenodd"
      stroke="#FFF"
      strokeLinecap="round"
      strokeWidth="2"
    >
      <path d="M1.197 7.591l14.78 6.552M30.803 7.591l-14.78 6.552M1.197 11.877l14.78 6.552M30.803 11.877l-14.78 6.552M1.223 16.153L16.007 22.7M30.833 16.153L16.053 22.7M1.223 20.438l14.784 6.552M30.833 20.438l-14.78 6.552M30.777 7.585L15.993 1.033M1.167 7.562l14.78-6.552" />
    </g>
  </svg>
)

import React from "react"

export default props => (
  <svg viewBox="0 0 6 14" {...props}>
    <path
      fillRule="evenodd"
      d="M1.251 13.748h2.501v-6.87H5.63L6 4.73H3.752V3.175c0-.501.325-1.028.788-1.028h1.277V0H4.252v.01C1.802.099 1.3 1.517 1.256 3.007H1.25v1.724H0v2.146h1.251v6.87z"
    />
  </svg>
)

import React from "react"

export default props => (
  <svg viewBox="0 0 10 6" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.83333 -5.09968e-08L10 1.13516L5.00013 6L-4.96195e-08 1.13516L1.16667 -3.86117e-07L5.00013 3.72968L8.83333 -5.09968e-08Z"
    />
  </svg>
)

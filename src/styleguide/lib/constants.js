export const GRID_SPACING_MOBILE = '.4rem';
export const GRID_SPACING_DESKTOP = '1rem';
export const DESKTOP_OR_LAPTOP_WIDTH = 1224;
export const BIG_SCREEN_WIDTH = 1824;

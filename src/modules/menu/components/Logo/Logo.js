import React from 'react';

const Logo = () => (
  <img src="/logo.svg" alt="NSC logo" width="78" height="34" />
);

export default Logo;

import React from 'react';
import styled from 'styled-components';

const Section = styled.div`
  width: 100%;
  height: 400px;
  position: relative;
`;

const Content = styled.div`
  position: relative;
  top: 40%;
  font-size: 50px;
  text-align: center;
  color: #ffffff;
  font-weight: bold;
`;

const Countdown = () => (
  <>
    <div className="container">
      <ul className="flip minutePlay">
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">0</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">0</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">1</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">1</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">2</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">2</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">3</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">3</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">4</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">4</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">5</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">5</div>
            </div>
          </a>
        </li>
      </ul>
      <ul className="flip secondPlay">
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">0</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">0</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">1</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">1</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">2</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">2</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">3</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">3</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">4</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">4</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">5</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">5</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">6</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">6</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">7</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">7</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">8</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">8</div>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div className="up">
              <div className="shadow"></div>
              <div className="inn">9</div>
            </div>
            <div className="down">
              <div className="shadow"></div>
              <div className="inn">9</div>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </>
);

export default Countdown;

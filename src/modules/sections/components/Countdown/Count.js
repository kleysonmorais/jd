import React, { Component } from 'react';
import styled from 'styled-components';
import FlipNumbers from 'react-flip-numbers';
import { Container } from 'react-bootstrap';

const Wrap = styled.div`
  margin-top: 5%;
  margin-bottom: 5%;
  text-align: center;
`;

class Count extends Component {
  state = {
    timeRemaining: 0,
  };

  componentDidMount() {
    this.timer = setInterval(() => {
      const { timeRemaining } = this.state;
      this.setState({
        timeRemaining: timeRemaining + 1,
      });
    }, 3000);
  }

  render() {
    const { timeRemaining } = this.state;
    return (
      <Wrap>
        <Container>
          <p>
            A taxa de fechamento de escolas no Brasil é de aproximadamente uma a
            cada 1h03min. Desde que esta história foi publicada, estima-se que
            Xx escolas foram fechadas. A próxima escola será encerrada em
            XXmmSS.
          </p>
          <FlipNumbers
            height={50}
            width={50}
            color="white"
            background="#330136"
            play="true"
            perspective={360}
            numbers={`${timeRemaining}`}
          />
        </Container>
      </Wrap>
    );
  }
}

export default Count;
